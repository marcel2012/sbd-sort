#include <stdlib.h>
#include "helpers.h"
#include "record_layer.h"


int rand_range(const int min, const int max) {
    return rand() % (max - min + 1) + min;
}

struct vector generate_random_record() {
    struct vector v;
    v.x = rand_range(-32000, 32000);
    v.y = rand_range(-32000, 32000);

    return v;
}

void swap_tapes(struct tape **t1, struct tape **t2) {
    struct tape *t3 = *t1;
    *t1 = *t2;
    *t2 = t3;
}

void generate_random_data(struct tape *tape, int n) {
    erase_tape_and_start_writing(tape);

    while (n--) {
        RECORD_TYPE record = generate_random_record();
        push_record(tape, &record);
    }
}

void print_tape(struct tape *tape) {
    print_tape_from_index(tape, 0);
}

void print_tape_from_index(struct tape *tape, long index) {
    printf("Reading tape %s (start index %ld): ", tape->file_name, index);

    struct vector *record = get_record(tape, index);
    struct vector previous_record;
    bool previous_record_exists = false;
    long runs = 1;
    long identical = 0;

    while (record != NULL) {
        if (previous_record_exists) {
            int c = cmp(&previous_record, record);
            if (c > 0) {
                runs++;
                printf(" || ");
            } else {
                printf(", ");
                if(c == 0) {
                    identical++;
                }
            }
        }
        printf("(%d, %d) %d", record->x, record->y, length_square(record));

        index++;
        previous_record = *record;
        previous_record_exists = true;
        record = get_record(tape, index);
    }
    printf("\nruns on tape: %ld, total number of records: %ld, identical records: %lu\n\n", runs, index, identical);
}