#ifndef MERGE_LAYER_H
#define MERGE_LAYER_H

#include "disk_layer.h"


struct tape *merge(struct tape **t_in, struct tape *t_out, int empty_runs, int empty_runs_tape_index, int *merge_steps);

void distribute(struct tape *t_in, struct tape **t_out, int *empty_runs, int *empty_runs_tape_index);

#endif
