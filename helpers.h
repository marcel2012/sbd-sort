#ifndef HELPERS_H
#define HELPERS_H

#include "record.h"
#include "disk_layer.h"


struct vector generate_random_record();

void swap_tapes(struct tape **t1, struct tape **t2);

void generate_random_data(struct tape *tape, int n);

void print_tape(struct tape *tape);

void print_tape_from_index(struct tape *tape, long index);

#endif
