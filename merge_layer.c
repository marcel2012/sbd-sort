#include "merge_layer.h"
#include "helpers.h"
#include "record_layer.h"


void push_and_get_next_record(struct tape *t_out, struct tape *t_in, RECORD_TYPE **record, long *index, bool *end_run) {
    push_record(t_out, *record);
    (*index)++;
    RECORD_TYPE previous_record = **record;
    *record = get_record(t_in, *index);
    if (*record == NULL || cmp(&previous_record, *record) > 0) {
        *end_run = true;
    }
}

struct tape *
merge(struct tape **t_in, struct tape *t_out, int empty_runs, int empty_runs_tape_index, int *merge_steps) {
    printf("Merging...\n");
    printf("Empty runs %d, tape index %d\n", empty_runs, empty_runs_tape_index);

    struct tape_helper {
        long index;
        RECORD_TYPE *record;
        bool end_run;
    };

    erase_tape_and_start_writing(t_out);

    struct tape_helper tapes[2];

    for (int i = 0; i < 2; i++) {
        tapes[i].index = 0;
        tapes[i].record = get_record(t_in[i], tapes[i].index);
        tapes[i].end_run = tapes[i].record == NULL;
    }

    empty_runs_tape_index = !empty_runs_tape_index;
    while (empty_runs--) {
        while (!tapes[empty_runs_tape_index].end_run) {
            push_and_get_next_record(t_out, t_in[empty_runs_tape_index], &tapes[empty_runs_tape_index].record,
                                     &tapes[empty_runs_tape_index].index, &tapes[empty_runs_tape_index].end_run);
        }
        tapes[empty_runs_tape_index].end_run = tapes[empty_runs_tape_index].record == NULL;
    }

    while (true) {
        do {
            while (!tapes[0].end_run && !tapes[1].end_run) {
                int index = cmp(tapes[0].record, tapes[1].record) > 0 ? 1 : 0;
                push_and_get_next_record(t_out, t_in[index], &tapes[index].record, &tapes[index].index,
                                         &tapes[index].end_run);
            }

            for (int index = 0; index < 2; index++) {
                while (!tapes[index].end_run) {
                    push_and_get_next_record(t_out, t_in[index], &tapes[index].record, &tapes[index].index,
                                             &tapes[index].end_run);
                }
                tapes[index].end_run = tapes[index].record == NULL;
            }
        } while (!tapes[0].end_run && !tapes[1].end_run);

        (*merge_steps)++;

        if (tapes[0].end_run && tapes[1].end_run) {
            for (int i = 0; i < 2; i++) {
                // clear file
                erase_tape_and_start_writing(t_in[i]);
            }

            break;
        } else {
            int index = tapes[1].end_run ? 1 : 0;

            tapes[index].index = 0;

#ifdef PRINT_TAPES
            printf("Tapes before swap ---BEGIN---\n");
            print_tape(t_out);
            print_tape_from_index(t_in[!index], tapes[!index].index);
            printf("Tapes before swap ---END---\n\n\n");

            // print changes buffer, need to fetch record
            tapes[!index].record = get_record(t_in[!index], tapes[!index].index);
#endif

            swap_tapes(t_in + index, &t_out);

            erase_tape_and_start_writing(t_out);

            tapes[index].record = get_record(t_in[index], tapes[index].index);
            tapes[index].end_run = tapes[index].record == NULL;
        }
    }

    return t_out;
}

void distribute(struct tape *t_in, struct tape **t_out, int *empty_runs, int *empty_runs_tape_index) {
    printf("Distributing...\n");

    struct tape_helper {
        RECORD_TYPE previous_record;
        bool previous_exists;
        int runs_counter;
    };

    struct tape_helper tapes[2];

    for (int i = 0; i < 2; i++) {
        erase_tape_and_start_writing(t_out[i]);
        tapes[i].runs_counter = 0;
        tapes[i].previous_exists = false;
    }

    long record_index = 0;
    int tape_index = 0;
    int runs_to_copy = 1;

    RECORD_TYPE *record = get_record(t_in, record_index);

    while (record != NULL) {
        int c = 0;
        if (tapes[tape_index].previous_exists) {
            c = cmp(&tapes[tape_index].previous_record, record);
        }

        if (c > 0) {
            tapes[tape_index].runs_counter++;

            runs_to_copy--;
            if (runs_to_copy == 0) {
                runs_to_copy = tapes[tape_index].runs_counter;
                tape_index = !tape_index;

                // coalescing

                if (tapes[tape_index].previous_exists && cmp(&tapes[tape_index].previous_record, record) <= 0) {
                    tapes[tape_index].runs_counter--;
                    runs_to_copy++;
                }
                tapes[tape_index].previous_exists = false;

                continue;
            }
        }

        push_record(t_out[tape_index], record);

        record_index++;
        tapes[tape_index].previous_record = *record;
        tapes[tape_index].previous_exists = true;
        record = get_record(t_in, record_index);
    }

    if (record_index) {
        tapes[tape_index].runs_counter++;
    }
    runs_to_copy--;

    if (runs_to_copy == tapes[!tape_index].runs_counter) {
        *empty_runs = 0;
    } else {
        *empty_runs = runs_to_copy;
    }

    *empty_runs_tape_index = tape_index;
}
