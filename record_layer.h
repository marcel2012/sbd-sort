#ifndef RECORD_LAYER_H
#define RECORD_LAYER_H

#include "record.h"
#include "disk_layer.h"


void push_record(struct tape *tape, const RECORD_TYPE *record);

RECORD_TYPE *get_record(struct tape *tape, const long index);

#endif
