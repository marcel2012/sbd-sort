#ifndef RECORD_H
#define RECORD_H


struct vector {
    int x, y;
};

typedef struct vector RECORD_TYPE;

int length_square(const struct vector *v);

int cmp(const struct vector *v1, const struct vector *v2);

#endif
