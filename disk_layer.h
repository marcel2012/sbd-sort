#ifndef DISK_LAYER_H
#define DISK_LAYER_H

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include "record.h"


struct page_header {
    long records;
};

#define PAGE_HEADER_SIZE sizeof(struct page_header)

#define PAGE_SIZE 512
#define RECORD_SIZE sizeof(RECORD_TYPE)
#define RECORDS_PER_PAGE ((PAGE_SIZE-PAGE_HEADER_SIZE)/RECORD_SIZE)

#define FILE_NAME_SIZE 15


struct tape {
    FILE *file;
    char file_name[FILE_NAME_SIZE];

    bool buffer_edited;
    long buffer_page_index;

    long last_record;

    uint8_t buffer[PAGE_SIZE];

    // easier access to buffer
    struct page_header *page_header;
    RECORD_TYPE *records;
};

int get_write_page_counter();

int get_read_page_counter();

void reset_counters();

struct tape *prepare_file(const char *file_name);

void erase_tape_and_start_writing(struct tape *tape);

void write_page(const struct tape *tape);

void write_page_if_edited(struct tape *tape);

void close_file(struct tape *tape);

bool read_page(struct tape *t, const long page_index);

#endif
