#include "record.h"


int length_square(const struct vector *v) {
    return v->x * v->x + v->y * v->y;
}

int cmp(const struct vector *v1, const struct vector *v2) {
    const int v1_length_square = length_square(v1);
    const int v2_length_square = length_square(v2);

    // printf("cmp %d %d\n", v1_length_square, v2_length_square);

    if (v1_length_square > v2_length_square) {
        return 1;
    } else if (v1_length_square == v2_length_square) {
        return 0;
    } else {
        return -1;
    }
}