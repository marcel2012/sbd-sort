#include <stdio.h>
#include <assert.h>
#include <time.h>
#include <stdlib.h>
#include "disk_layer.h"
#include "helpers.h"
#include "merge_layer.h"
#include "record_layer.h"


int main() {
    srand(time(0));

    printf("Records per page: %lu\n", RECORDS_PER_PAGE);
    printf("Record size: %lu bytes\n", RECORD_SIZE);

    assert(RECORDS_PER_PAGE > 0);

    struct tape *tape1 = prepare_file("t1.dat");
    struct tape *tape2 = prepare_file("t2.dat");
    struct tape *tape3 = prepare_file("t3.dat");

    printf("Podaj tryb programu:\n");
    printf("\t1) Dane losowe\n");
    printf("\t2) Dane ze standardowego wejścia/pliku\n");

    int mode, number_of_records;
    int status;

    status = scanf("%d", &mode);

    assert(status == 1 && mode >= 1 && mode <= 2);

    printf("Podaj liczbę rekordów:\n");
    status = scanf("%d", &number_of_records);
    assert(status == 1 && number_of_records >= 0);

    if (mode == 1) {
        generate_random_data(tape1, number_of_records);
    } else {
        printf("Podaj rekordy w postaci\n");
        printf("x y\n");

        erase_tape_and_start_writing(tape1);

        while (number_of_records--) {
            RECORD_TYPE record;
            status = scanf("%d%d", &record.x, &record.y);
            assert(status == 2);
            push_record(tape1, &record);
        }
    }

#ifdef PRINT_TAPES
    print_tape(tape1);
#endif


    // clear cache
    write_page_if_edited(tape1);
    tape1->buffer_page_index = -1;

    reset_counters();

    int empty_runs, empty_runs_tape_index;
    int merge_steps = 0;
    struct tape *tapes[] = {tape2, tape3};

    distribute(tape1, tapes, &empty_runs, &empty_runs_tape_index);

#ifdef PRINT_TAPES
    printf("Tapes after distribution ---BEGIN---\n");
    print_tape(tape2);
    print_tape(tape3);
    printf("Tapes after distribution ---END---\n\n\n");
#endif

    struct tape *t_out = merge(tapes, tape1, empty_runs, empty_runs_tape_index, &merge_steps);

#ifdef PRINT_TAPES
    printf("Final tape ---BEGIN---\n");
    print_tape(t_out);
    printf("Final tape ---END---\n");
#endif

    close_file(tape1);
    close_file(tape2);
    close_file(tape3);

    printf("Page reads %d, writes %d, merge steps %d\n", get_read_page_counter(), get_write_page_counter(), merge_steps);

    return 0;
}
